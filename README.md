# Prerequisites

- Java 21

# Project

- **sp-keycloak**: this module is the **Service Provider** and contain a embedded keycloak server (http://localhost:8085/auth/)     
  - User: sp-admin
  - Pass: sp-pass
- **idp-keycloak**: this module is the **Identity Provider** and contain a embedded keycloak server (http://localhost:9095/auth/)
  - User: idp-admin 
  - Pass: idp-pass

**NOTE**: To avoid the keycloak session logout, open one server in a incognito tab.

# SAML Configuration with Keycloak
In this example, we will use keycloak to generate the Service Provider (SP) and Identity Provider (IDP).

![images](images/servers.png)

Outcome expected: Users of the IDP should be able to access SP Keycloak server console via SAML protocol.

Implementation steps:
1. Import IDP metadata to the SP
2. Import SP metadata to the IDP 
3. Grant admin privileges to the users coming from the IDP

## Import IDP metadata to the SP
- In the SP server. Create a new realm in the SP with the name “service-provider-realm”.
- In the SP server. Go to Identity Provider section and configure a SAML v2.0 provider
  - Display Name: SAML IDP 

![images](images/import idp 001.png)

- In the IDP server. Create a new realm “identity-provider-realm” 

![images](images/import idp 002.png)

- In the IDP server. Copy the “SAML 2.0 Identity Provider Metadata” link 

![images](images/import idp 003.png)

- In the SP server. Paste the link in “Import from URL” and click “Add”

![images](images/import idp 004.png)

## Import SP metadata to the IDP
- We need to download the SAML metadata from the SP 
- In the SP server. Go to the SAML IDP and “Save link as..” sp-metadata.xml file

![images](images/import sp 001.png)

- After save the file, we need to import it in the IDP server 
- In the IDP server. Go to the client section and Import client

![images](images/import sp 002.png)

- In the IDP server. Select the sp-metadata.xml file and it will fill the client data

![images](images/import sp 003.png)

- In the IDP server. Save the new client

![images](images/import sp 004.png)

## Grant admin privileges to the users coming from the IDP
- We need to configure a mapper in the SP. 

![images](images/grant 001.png)

- In the IDP server. Select SAML IDP and create a new mapper 
  - Name: admin-role-mapper 
  - Mapper Type: Hardcoded Role 
  - Role: Select Role → Filter by Clients → Search “admin” → Select “realm-admin”

![images](images/grant 002.png)
![images](images/grant 003.png)

## Test the solution
- Create a new user in the IDP server 
  - Username: idp-user 
  - Set password: idp-pass 

![images](images/test 001.png)

- Now we will login in the SP realm using IDP authentication 
- In the SP server. Go to the client section and click in the console url 

![images](images/test 002.png)

- We can see the SAML IDP option 

![images](images/test 003.png)

- When click in the button, we will redirect to IDP server and login with idp-user 

![images](images/test 004.png)

- In the first login we need fill additional information 

![images](images/test 005.png)

- And the login into SP using IDP  is done!

![images](images/test 006.png)

